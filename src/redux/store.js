import {createStore, combineReducers} from 'redux';
import * as reducer from './reducer'
/*=================
    store.jsx
  中央数据处理器
==================*/
var store = createStore(
    combineReducers(reducer)
);
export default store;