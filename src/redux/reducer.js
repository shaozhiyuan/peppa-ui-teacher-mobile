/*=================
 reducer.jsx
 接收Action 并作出处理
 ==================*/
export const GetInitData = (state = {data:{}}, action = {}) => {
    switch (action.type) {
        case 'DispatchInitData':
            return state.set('initData',action.data);
        default:
            return state;
    }
};