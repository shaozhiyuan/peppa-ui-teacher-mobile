import React, { Component } from 'react';
import MainTab from '../component/tab/MainTab'
import { Tabs } from 'antd-mobile';
import './student_core.less'


class StudentCore extends Component {
    constructor() {
        super();
        this.state = {
            tabs: [
                {title: '作业批改'},
                {title: '学员测评'}
            ]
        }
    }
    render(){
        return (
            <div id="student-core">
                <header>
                    <div className="student-tab-wrap">
                        <Tabs tabs={this.state.tabs} initialPage={1} animated={false} useOnPan={false}>
                            <div>
                                Content of first tab
                            </div>
                            <div>
                                Content of second tab
                            </div>
                        </Tabs>
                    </div>
                </header>
                <MainTab/>
            </div>
        )
    }
}


export default StudentCore