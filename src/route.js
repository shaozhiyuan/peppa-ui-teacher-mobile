import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

import StudentCore from './student_core/StudentCore'
import MySchedule from './my_schedule/MySchedule'
import PersonalCore from './personal_core/PersonalCore'

export default <Switch>
    <Route exact path='/personal-core' component={PersonalCore}/>
    <Route exact path='/my-course' component={MySchedule}/>
    <Route exact path='/student-core' component={StudentCore}/>
    <Redirect to="/student-core" />
</Switch>