import React, {Component} from 'react';

class DefaultFallbackComponent extends React.PureComponent {
    handleBackToHomeClick = (event) => {
        event.preventDefault();
        window.location.href = '/';
    };
    render() {
        return (
            <div style={{
                textAlign: 'center',
                padding: '30px 10px'
            }}>
                发生未知错误, 回<a href="javacript:;" onClick={this.handleBackToHomeClick}>首页</a>
            </div>
        );
    }
}

class ErrorBoundary extends Component {
    static defaultProps = {
        fallback: DefaultFallbackComponent
    };
    constructor (props) {
        super(props);
        this.state = {
            error: false
        };
    }
    componentDidCatch(error) {
        this.setState({
            error
        });
        console.log(error)
    }
    render() {
        const { error } = this.state;
        const { fallback: FallbackComponent } = this.props;
        return error ? <FallbackComponent /> : this.props.children;
    }
}

export default ErrorBoundary;