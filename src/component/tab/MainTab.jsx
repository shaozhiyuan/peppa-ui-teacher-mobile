import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import './main_tab.less'



class MainTab extends Component{
    render() {
        return (
            <div className="main-tab-bar">
                <div>
                    <NavLink exact to="/my-course">

                        <p>我的课表</p>
                    </NavLink>
                </div>
                <div>
                    <NavLink exact to="/student-core">
                        <p>学员中心</p>
                    </NavLink>
                </div>
                <div>
                    <NavLink exact to="/personal-core">
                        <p>个人中心</p>
                    </NavLink>
                </div>
            </div>
        )
    }
}


export default MainTab

