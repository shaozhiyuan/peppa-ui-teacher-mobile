import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import routes from './route';
import {Provider} from 'react-redux';
import store from './redux/store'
import './app.less';
import 'antd-mobile/dist/antd-mobile.css';
import ErrorBoundary from './component/error/ErrorBoundary';

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
          <Provider store={store}>
              <BrowserRouter>
                  {routes}
              </BrowserRouter>
          </Provider>
      </ErrorBoundary>
    );
  }
}

export default App;
